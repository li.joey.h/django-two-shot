from django.contrib import admin
from receipts.models import Account, ExpenseCategory, Receipt


@admin.register(Account)
class AccountAdmin(admin.ModelAdmin):
    list_display = [
        "name",
        "number",
        "owner",
        "id"
    ]


@admin.register(ExpenseCategory)
class ExpenseCategoryAdmin(admin.ModelAdmin):
    list_display = [
        "name",
        "owner",
        "id"
    ]


@admin.register(Receipt)
class Receipt(admin.ModelAdmin):
    list_display = [
        "vendor",
        "total",
        "tax",
        "date",
        "purchaser",
        "category",
        "account",
        "id"
    ]
